#!/bin/bash

if [[ $UID -ne 0 ]]; then
    echo "Error: Must be run with elevated privileges."
    exit 1
fi
rsyncd_conf=/etc/rsyncd.conf
rsyncd_dir=/srv/rsync
rsyncd_name=share

# Ensure that rsync is installed.
if ! which rsync >/dev/null; then
    echo "Installing rsync"
    apt-get install --yes rsync
fi

# Ensure rsyncd dir exists.
if [[ ! -d $rsyncd_dir ]]; then
    echo "Creating rsync dir: $rsync_dir"
    mkdir -pm777 $rsyncd_dir
fi

# Ensure basic rsyncd config.
config="
[$rsyncd_name]
    path = $rsyncd_dir
    comment = shared files
    read only = true
    max connections = 5
    dont compress = *.gz *.tgz *.zip *.z *.Z *.rpm *.deb *.bz2 *.snap *.xz

"
# Ensure conf file exists
if [[ ! -f "$rsyncd_conf" ]]; then
    echo "Creating $rsyncd_conf"
    touch "$rsyncd_conf"
fi
# Ensure base share dir is added.
if [[ ! $(grep "\[${rsyncd_name}\]" "$rsyncd_conf") ]]; then
    echo "Adding $rsyncd_dir to $rsyncd_conf"
    echo "$config" | tee -a "$rsyncd_conf"
fi
# Ensure daemon is running.
if [[ $(systemctl is-active rsync.service) != 'active' ]]; then
    echo "Starting rsyncd"
    systemctl enable --now rsync.service
fi