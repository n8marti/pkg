#!/bin/bash

user=nate
parent="$(dirname "$0")"

if [[ -z $UID -ne 0 ]]; then
    echo "Error: Must be run with elevated privileges."
    exit 1
fi
apt-get update && apt-get --yes upgrade

echo "Ensuring snapd."
if ! which snap >/dev/null; then
    echo "Installing snapd"
    apt-get --yes install snapd
fi

echo "Ensuring lxd."
if ! which lxd >/dev/null; then
    echo "Installing lxd"
    snap install lxd
fi

echo "Ensuring user exists & has correct group memberships."
if ! getent passwd "$user" >/dev/null; then
    adduser "$user" --comment ''
fi
for group in adm cdrom sudo dip plugdev lxd; do
    adduser "$user" $group
done

echo "Ensuring rsyncd sharing."
"${parent}/ensure-rsyncd.sh"

echo "Current IP info:"
ip -br a

echo "SSHd config (after adding ~/.ssh/id_***.pub contents to /home/nate/.ssh/authorized_keys):"
echo "
PermitRootLogin no
PasswordAuthentication no
UsePAM yes
AllowUsers nate
"